// Importing function

const cacheFunction = require("../cacheFunction.js");

// Cube function
function cube(element) {
  return element * element * element
}

let func = cacheFunction(cube);

// Argument Data
let arrData = [1, 2, 3, 1, 1, 6, 6, 7, 8, 9, 1, 2];

let returnedValue;

console.log('\n************************************')
console.log('Testing with callback function: Cube')
console.log('************************************\n')

console.log('')

for (let i = 0; i < arrData.length; i++) {
  returnedValue = func(arrData[i]);
  console.log(returnedValue);
}
