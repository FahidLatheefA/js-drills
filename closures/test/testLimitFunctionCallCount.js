// Importing function

let limitFunction = require("../limitFunctionCallCount");

let cb = function displayFunction() {
    console.log(`Calling function`);
};

let limit = limitFunction(cb, 8);

for (let i = 0; i < 10; i++) {
    process.stdout.write(`${i+1}: `);
    limit()
}
