function limitFunctionCallCount(cb, n) {
    // Should return a function that invokes `cb`.
    // The returned function should only allow `cb` to be invoked `n` times.
    // Returning null is acceptable if cb can't be returned
    let count = n;
    return function limit() {
        if (count > 0) {
            count -= 1;
            cb();
        } else {
            console.log(`Function already invoked ${n} times`)
        }
    };
};

module.exports = limitFunctionCallCount;
