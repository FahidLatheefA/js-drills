function cacheFunction(cb) {
    // Should return a function that invokes `cb`.
    // A cache (object) should be kept in closure scope.
    // The cache should keep track of all arguments have been used to invoke this function.
    // If the returned function is invoked with arguments that it has already seen
    // then it should return the cached result and not invoke `cb` again.
    // `cb` should only ever be invoked once for a given set of arguments.

    let objectCache = {};

    return function test(argument) {
        if (objectCache[argument]) {

            return `Argument ${argument} repeated, Invoking from cache => ${objectCache[argument]}\n`;

        } else {

            objectCache[argument] = cb(argument);
            return `Caching new Argument ${argument}. Output => ${objectCache[argument]}\n`;
        }
    };
}

module.exports = cacheFunction;
