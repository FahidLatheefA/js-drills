// function to display car years
function displayCarYears(inventory) {
    let yearList = [];
    for (let i = 0; i < inventory.length; i++) {
        yearList.push(inventory[i].car_year);
    };
    return yearList;
};

// Exporting the function
export {
    displayCarYears
};
