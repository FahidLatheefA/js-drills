import {
    inventory
} from '../cars.js';

import {
    findCarById
} from '../problem1.js';

//Manual Testing

let result = findCarById(inventory, 33);
console.log(`Car ${result.id} is a ${result.car_year} ${result.car_make} ${result.car_model}`);

if (result.car_make == 'Jeep' &&
    result.car_model == 'Wrangler' &&
    result.car_year == 2011) {
    console.log("Testing Successful");
} else {
    console.log("Testing Failed");
};
