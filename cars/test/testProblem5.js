import {
    inventory
} from '../cars.js';

import {
    finalOutput
} from '../problem5.js';

let result = finalOutput(inventory);
console.log(result);
console.log(`Number of old cars before year 2000 is ${result.length}`);

let expectedResult = [
    1983, 1990, 1995, 1987, 1996,
    1997, 1999, 1987, 1995, 1994,
    1985, 1997, 1992, 1993, 1964,
    1999, 1991, 1997, 1992, 1998,
    1965, 1996, 1995, 1996, 1999
];

//Manual Testing

if (JSON.stringify(result) == JSON.stringify(expectedResult) &&
    result.length === 25) {
    console.log("Testing Successful");
} else {
    console.log("Testing Failed");
};
