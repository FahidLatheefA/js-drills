import {
    inventory
} from '../cars.js';

import {
    findLastCar
} from '../problem2.js';

//Manual Testing

let result = findLastCar(inventory);
console.log(`Last car is a ${result.car_make} ${result.car_model}`);

if (result.car_model == 'Town Car' &&
    result.car_make == 'Lincoln') {
    console.log("Testing Successful");
} else {
    console.log("Testing Failed");
};
