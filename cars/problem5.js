// function to display car years
function displayCarYears(inventory) {
    let yearList = [];
    for (let i = 0; i < inventory.length; i++) {
        yearList.push(inventory[i].car_year);
    };
    return yearList;
};

function finalOutput(inventory) {
    let data = displayCarYears(inventory);
    let oldCars = [];
    for (let i = 0; i < data.length; i++) {
        if (data[i] < 2000) {
            oldCars.push(data[i])
        };
    };
    return oldCars;
}

// Exporting the function
export {
    finalOutput
};
