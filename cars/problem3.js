// Function for case insensitive sort
function sorter(a, b) {
    if (a === b) {
        return 0;
    };
    if (a.toLowerCase() < b.toLowerCase()) {
        return -1;
    };
    if (a.toLowerCase() > b.toLowerCase()) {
        return 1;
    };
};


// function to display models
function displayCarModels(inventory) {
    let modelList = [];
    for (let i = 0; i < inventory.length; i++) {
        modelList.push(inventory[i].car_model);
    }
    modelList.sort(sorter);
    return modelList;
};

// Exporting the function
export {
    displayCarModels
};
