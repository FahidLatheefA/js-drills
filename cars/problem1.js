function findCarById(inventory, idValue) {
    for (let i = 0; i < inventory.length; i++) {
        if (inventory[i].id == idValue) {
            return inventory[i];
        };
    };
};

// Exporting the function
export {
    findCarById
};
