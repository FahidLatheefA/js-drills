function findAllBMWAndAudi(inventory) {
    let newCarList = [];
    for (let i = 0; i < inventory.length; i++) {
        if (inventory[i].car_make == 'Audi' || inventory[i].car_make == 'BMW') {
            newCarList.push(inventory[i]);
        };
    };
    return newCarList;
};

function finalOutput(inventory) {
    let data = findAllBMWAndAudi(inventory);
    for (let i = 0; i < data.length; i++) {
        console.log(JSON.stringify(data[i]));
    };
    return data;
};

// Exporting the function
export {
    finalOutput
};
