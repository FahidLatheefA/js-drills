function findLastCar(inventory) {
    let lastCar = inventory.slice(-1);
    return lastCar[0];
};

// Exporting the function
export {
    findLastCar
};
