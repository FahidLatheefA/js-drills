// Function to emulate reduce

function reduce(list, cb) {
    if (list.length === 1) {
        return list[0];
    };
    let joiner = cb(list[0], list[1]);
    let out = joiner;
    for (let i = 1; i < list.length - 1; i++) {
        joiner = cb(out, list[i + 1]);
        out = joiner;
    };
    return out;
};

// Exporting the function
export {
    reduce
};
