// Function to emulate find
// Returns the first instance

function find(list, cb) {
    for (let i = 0; i < list.length; i++) {
        if (cb(list[i]) == true) {
            return list[i];
        };
    };
    return undefined;
};

// Exporting the function
export {
    find
};
