// Function to emulate filter

function filter(list, cb) {
    let output = [];
    for (let i = 0; i < list.length; i++) {
        if (cb(list[i]) == true) {
            output.push(list[i]);
        };
    };
    return output;
};

// Exporting the function
export {
    filter
};
