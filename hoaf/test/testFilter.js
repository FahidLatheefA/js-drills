// Importing function

import {
    filter
} from '../filter.js';

const items = [1, 2, 3, 4, 5, 5];

// Create a truth test of even numbers
function isEven(element) {
    if (element % 2 === 0) {
        return true;
    }
    return false;
};

console.log("------------");
console.log("Testing Filter");
console.log("------------");
console.log(`Input: [${items}]\n`);
console.log("Callback Function: isEven");
console.log(`\nOutput:`);
let result = filter(items, isEven);
console.log(result);
console.log("\nExpected Output:");
let expectedResult = [2, 4]; //Expected Result
console.log(`[${expectedResult}]\n`);
if (JSON.stringify(result) == JSON.stringify(expectedResult)) {
    console.log("Testing Successful");
} else {
    console.log("Testing Failed");
};
console.log('\n****************************');
