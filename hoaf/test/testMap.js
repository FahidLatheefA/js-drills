// Importing function

import {
    map
} from '../map.js';

const items = [1, 2, 3, 4, 5, 5];

// Create a function to triple the elements
function triple(element) {
    return element * 3;
};

console.log("------------");
console.log("Testing Map");
console.log("------------");
console.log(`Input: [${items}]\n`);
console.log("Callback Function: triple");
console.log(`\nOutput:`);
let result = map(items, triple);
console.log(result);
console.log("\nExpected Output:");
let expectedResult = [3, 6, 9, 12, 15, 15];
console.log(`[${expectedResult}]\n`)
if (JSON.stringify(result) == JSON.stringify(expectedResult)) {
    console.log("Testing Successful");
} else {
    console.log("Testing Failed");
};
console.log('\n****************************');
