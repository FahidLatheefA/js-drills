// Importing function

import {
    each
} from '../each.js';

const items = [1, 2, 3, 4, 5, 5];

// Create a function to triple the elements
function triple(element) {
    return element * 3;
};

console.log("------------");
console.log("Testing Each");
console.log("------------");
console.log(`Input: [${items}]\n`);
console.log("Callback Function: triple");
console.log(`\nOutput:`);
console.log(each(items, triple));
console.log('\n****************************');
