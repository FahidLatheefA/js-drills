// Importing function

import {
    reduce
} from '../reduce.js';

const items = [1, 2, 3, 4, 5, 5];

// Create a function to multiply 2 elements
function multiply(first, second) {
    return first * second;
};

console.log("------------");
console.log("Testing Reduce");
console.log("------------");
console.log(`Input: [${items}]\n`);
console.log("Callback Function: multiply");
console.log(`\nOutput:`);
let result = reduce(items, multiply);
console.log(result);
console.log("\nExpected Output:");
let expectedResult = 600; //Actual Result
console.log(`${expectedResult}\n`)
if (result == expectedResult) {
    console.log("Testing Successful");
} else {
    console.log("Testing Failed");
};
console.log('\n****************************');
