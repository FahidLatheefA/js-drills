// Importing function

import {
    flatten
} from '../flatten.js';

const nestedArray = [1, [2], [[3]], [[[4]]]];

console.log("------------");
console.log("Testing Flatten");
console.log("------------");
console.log(`Input: [1, [2], [[3]], [[[4]]]] \n`);
console.log(`\nOutput:`);
let result = flatten(nestedArray);
console.log(result);
console.log("\nExpected Output:");
let expectedResult = [1,2,3,4] //Expected Result
console.log(`[${expectedResult}]\n`);
if (JSON.stringify(result) == JSON.stringify(expectedResult)) {
    console.log("Testing Successful");
} else {
    console.log("Testing Failed");
};
console.log('\n****************************');
