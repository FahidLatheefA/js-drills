// Importing function

import {
    find
} from '../find.js';

const items = [1, 2, 3, 4, 5, 5];

// Create a function of even numbers
function isEven(element) {
    if (element % 2 === 0) {
        return true;
    }
    return false;
};

console.log("------------");
console.log("Testing Find");
console.log("------------");
console.log(`Input: [${items}]\n`);
console.log("Callback Function: isEven");
console.log(`Finds First Even Number in the list`);
console.log(`\nOutput:`);
let result = find(items, isEven);
console.log(result);
console.log("\nExpected Output:");
let expectedResult = 2 //Expected Result
console.log(`${expectedResult}\n`);
if (result == expectedResult) {
    console.log("Testing Successful");
} else {
    console.log("Testing Failed");
};
console.log('\n****************************');
