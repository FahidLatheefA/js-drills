// Function to emulate map

function map(list, cb) {
    let output = [];
    for (let i = 0; i < list.length; i++) {
        output.push(cb(list[i], i));
    };
    return output;
};

// Exporting the function
export {
    map
};
