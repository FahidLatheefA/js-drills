// Function to emulate flatten

function flatten(list, result = []) {
  for (let i = 0, length = list.length; i < length; i++) {
    const value = list[i];//First element
    //Check if first element is an array
    if (Array.isArray(value)) {
      flatten(value, result); //recursion till element is not array
    } else {
      result.push(value); // Updating output
    };
  };
  return result;
};

// Exporting the function
export {
  flatten
};
