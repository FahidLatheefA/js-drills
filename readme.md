These are the solution to the Javascript Drills with MountBlue

## Folder Structure
`cars`: Cars Drills

`hoaf`: Higher Order Array Functions Drills

`objects`: Objects Drills

`closures`: Closures Drills

`callbacks`: Callbacks Drills
