// Importing keys
import {
    keys
} from './keys.js';

// Importing values
import {
    values
} from './values.js';

// Function to emulate mapObject in object
// Works with values
function mapObject(obj, cb) {
    let out = {};
    let valuesList = values(obj);
    let keysList = keys(obj);
    for (let i = 0; i < keysList.length; i++) {
        let ele = keysList[i];
        out[ele] = cb(valuesList[i]);
    };
    return out;
};

// Exporting the function
export {
    mapObject
};
