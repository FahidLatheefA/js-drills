// Function to emulate keys in object

function keys(obj) {
    let out = [];
    for (let ele in obj) {
        out.push(ele);
    };
    return out;
}

// Exporting the function
export {
    keys
};
