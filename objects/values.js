// Importing keys
import {
    keys
} from './keys.js';

// Function to emulate values in object
function values(obj) {
    let out = [];
    let keysList = keys(obj);
    for (let i=0; i<keysList.length; i++) {
        out.push(obj[keysList[i]]);
    };
    return out;
}

// Exporting the function
export {
    values
};
