// Importing function

import {
    mapObject
} from '../mapObject.js';

// function to triple
function triple(ele) {
    return ele * 3
};

const testObject = {
    'h': 12,
    'i': 0,
    'j': 15,
    'k': 3,
    'l': 23
};

console.log("------------");
console.log("Testing mapObject");
console.log("------------");
console.log(`Input: {h: 12, i: 0, j: 15, k: 3, l: 23}\n`);
console.log(`\nOutput:`);
console.log("Callback Function: triple");
let result = mapObject(testObject, triple);
console.log(result);
console.log("\nExpected Output:");
let expectedResult = {h: 36, i: 0, j: 45, k: 9, l: 69}; //Expected Result
console.log(`{h: 36, i: 0, j: 45, k: 9, l: 69}\n`);
if (JSON.stringify(result) == JSON.stringify(expectedResult)) {
    console.log("Testing Successful");
} else {
    console.log("Testing Failed");
};
console.log('\n****************************');
