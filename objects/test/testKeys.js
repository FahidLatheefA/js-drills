// Importing function

import {
    keys
} from '../keys.js';

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

console.log("------------");
console.log("Testing keys");
console.log("------------");
console.log(`Input: { name: 'Bruce Wayne', age: 36, location: 'Gotham' }\n`);
console.log(`\nOutput:`);
let result = keys(testObject);
console.log(result);
console.log("\nExpected Output:");
let expectedResult = ['name', 'age', 'location']; //Expected Result
console.log(`[${expectedResult}]\n`);
if (JSON.stringify(result) == JSON.stringify(expectedResult)) {
    console.log("Testing Successful");
} else {
    console.log("Testing Failed");
};
console.log('\n****************************');
