// Importing function

import {
    defaults
} from '../defaults.js';

const testObject = {name: 'Bruce Wayne', age: 36, location: 'Gotham'};
const defaultProps = {name: 'Joker', age: 45, mental: 'unstable'};

console.log("------------");
console.log("Testing defaults");
console.log("------------");
console.log(`Input: {name: 'Bruce Wayne', age: 36, location: 'Gotham'}\n`);
console.log(`defaultProps: {name: 'Joker', age: 45, mental: 'unstable'}\n`);
console.log(`\nOutput:`);
let result = defaults(testObject, defaultProps);
console.log(result);
console.log("\nExpected Output:");
let expectedResult = {
    name: 'Bruce Wayne', age: 36, location: 'Gotham', mental: 'unstable'
  }; //Expected Result
console.log(`{'name':'Bruce Wayne' , 'age':'36',  'location':'Gotham', mental: 'unstable'}\n`);
if (JSON.stringify(result) == JSON.stringify(expectedResult)) {
    console.log("Testing Successful");
} else {
    console.log("Testing Failed");
};
console.log('\n****************************');
