// Importing function

import {
    pairs
} from '../pairs.js';

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

console.log("------------");
console.log("Testing pairs");
console.log("------------");
console.log(`Input: { name: 'Bruce Wayne', age: 36, location: 'Gotham' }\n`);
console.log(`\nOutput:`);
let result = pairs(testObject);
console.log(result);
console.log("\nExpected Output:");
let expectedResult = [['name', 'Bruce Wayne'], ['age', 36], ['location', 'Gotham']]; //Expected Result
console.log(`[['name', 'Bruce Wayne'], ['age', 36], ['location', 'Gotham']]\n`);
if (JSON.stringify(result) == JSON.stringify(expectedResult)) {
    console.log("Testing Successful");
} else {
    console.log("Testing Failed");
};
console.log('\n****************************');
