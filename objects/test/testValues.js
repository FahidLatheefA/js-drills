// Importing function

import {
    values
} from '../values.js';

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

console.log("------------");
console.log("Testing values");
console.log("------------");
console.log(`Input: { name: 'Bruce Wayne', age: 36, location: 'Gotham' }\n`);
console.log(`\nOutput:`);
let result = values(testObject);
console.log(result);
console.log("\nExpected Output:");
let expectedResult = ['Bruce Wayne', 36, 'Gotham']; //Expected Result
console.log(`[${expectedResult}]\n`);
if (JSON.stringify(result) == JSON.stringify(expectedResult)) {
    console.log("Testing Successful");
} else {
    console.log("Testing Failed");
};
console.log('\n****************************');
