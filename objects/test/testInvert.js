// Importing function

import {
    invert
} from '../invert.js';

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

console.log("------------");
console.log("Testing invert");
console.log("------------");
console.log(`Input: { name: 'Bruce Wayne', age: 36, location: 'Gotham' }\n`);
console.log(`\nOutput:`);
let result = invert(testObject);
console.log(result);
console.log("\nExpected Output:");
let expectedResult = {'Bruce Wayne': 'name', '36': 'age',  'Gotham': 'location'}; //Expected Result
console.log(`{'Bruce Wayne': 'name', '36': 'age',  'Gotham': 'location'}\n`);
if (JSON.stringify(result) == JSON.stringify(expectedResult)) {
    console.log("Testing Successful");
} else {
    console.log("Testing Failed");
};
console.log('\n****************************');
