// Importing keys
import {
    keys
} from './keys.js';

// Importing values
import {
    values
} from './values.js';

// Function to emulate pairs in object
function pairs(obj) {
    let out = [];
    let keysList = keys(obj);
    let valuesList = values(obj);

    for (let i=0; i<keysList.length;i++){
        out.push([keysList[i], valuesList[i]])
    }

    return out;
}

// Exporting the function
export {
    pairs
};
