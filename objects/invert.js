// Importing keys
import {
    keys
} from './keys.js';

// Importing values
import {
    values
} from './values.js';

// Function to emulate invert in object

function invert(obj) {
    let out = {};
    let keysList = keys(obj);
    let valuesList = values(obj);

    for (let i = 0; i < keysList.length; i++) {
        out[valuesList[i]] = keysList[i]
    }

    return out;
}


// Exporting the function
export {
    invert
};
