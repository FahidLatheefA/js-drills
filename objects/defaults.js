// Importing keys
import {
    keys
} from './keys.js';

// Importing values
import {
    values
} from './values.js';

// Function to emulate defaults in object
function defaults(obj, defaultProps) {
    let keysList = keys(defaultProps);
    let valuesList = values(defaultProps);

    for (let i = 0; i < keysList.length; i++) {
        if (!(keysList[i] in obj)) {
            obj[keysList[i]] = valuesList[i];
        }
    };

    return obj;
}

// Exporting the function
export {
    defaults
};
