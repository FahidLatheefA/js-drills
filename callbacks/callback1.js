/* 
  Problem 1: Write a function that will return a particular board's information 
  based on the boardID that is passed from the given list of boards in boards.json 
  and then pass control back to the code that called it by using 
  a callback function.
*/

// boards.json is an array

function callback1(listData, idData, cb) {
    setTimeout(() => {
        let data;
        for (let i = 0; i < listData.length; i++) {
            if (listData[i].id == idData) {
                data = listData[i];
                break;
            }
        }
        if (data) {
            cb(null, data)
        } else {
            cb("No data received", null)
        }
    }, 2 * 1000);
}

module.exports = {
    callback1
};