const objectData = require("../data/cards.json");
let callbackFunction = require("../callback3")

//Call by specific id
const ID = "ghnb768"
console.log(`\nReturns cards information:\n\nCalling by Specific ID "${ID}"`)
console.log("**********************************\n")

callbackFunction.callback3(objectData, ID, (err, data) => {
    if (err) {
        console.log(err);
    } else if (data) {
        console.log(data);
    }
});
