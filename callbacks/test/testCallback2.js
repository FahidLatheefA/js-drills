const objectData = require("../data/lists.json");
let callbackFunction = require("../callback2")

//Call by specific id
const ID = "abc122dc"
console.log(`\nReturns lists information:\n\nCalling by Specific ID "${ID}"`)
console.log("**********************************\n")

callbackFunction.callback2(objectData, ID, (err, data) => {
    if (err) {
        console.log(err);
    } else if (data) {
        console.log(data);
    }
});
