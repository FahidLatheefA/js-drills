const callbackFunction = require("../callback1");

const boardData = require("../data/boards.json");

//Call by specific id
const ID = "abc122dc"
console.log(`\nReturns boards information:\n\nCalling by Specific ID "${ID}"`)
console.log("**********************************\n")

callbackFunction.callback1(boardData, ID, (err, data) => {
    if (err) {
        console.log(err);
    } else if (data) {
        console.log(data);
    };
});
