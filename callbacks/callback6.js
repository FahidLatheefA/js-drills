/* 
Problem 6: Write a function 
that will use the previously written functions 
to get the following information. 
You do not need to pass control back to the code that called it.

Get information from the Thanos boards
Get all the lists for the Thanos board
Get all cards for all lists simultaneously 
*/

const boardsData = require("./data/boards.json");
const cardsData = require("./data/cards.json");
const listsData = require("./data/lists.json");
const cb1 = require("./callback1");
const cb2 = require("./callback2");
const cb3 = require("./callback3");

// Function to extract ID based on board name
function findBoardsID(boardName) {
    let boardID = "";

    for (let i = 0; i < boardsData.length; i++) {
        if (boardsData[i].name == boardName) {
            boardID = boardsData[i].id;
        };
    }
    return boardID;
}

// Function to capture the required data
const callback6 = (boardName) => {
    let boardsID = findBoardsID(boardName);

    setTimeout(() => {
        cb1.callback1(boardsData, boardsID, (err, data) => {
            if (err) {
                console.log(err)
            } else if (data) {
                console.log(`\nGet information from ${boardName} board`)
                console.log("*********************************\n")
                console.log(data);
                cb2.callback2(listsData, data.id, (err, data) => {
                    console.log(`\nGet all lists for ${boardName} board`)
                    console.log("******************************\n")
                    console.log(data)
                    if (err) {
                        console.log(err)
                    } else {
                        if (data) {
                            for (var i = 0; i < data.length; i++) {
                                let nameCard = data[i]['name'];
                                cb3.callback3(cardsData, data[i].id, (err, data) => {
                                    if (data) {
                                        console.log(`\nGet all cards for the ${nameCard} list`)
                                        console.log("*********************************\n")
                                        console.log(data);
                                    } else {
                                        console.log(`\nGet all cards for the ${nameCard} list`)
                                        console.log("*********************************\n")
                                        console.log(`No data found for ${nameCard} list\n`);
                                    }
                                });
                            }


                        }
                    }
                });
            }
        });

    }, 2000)
};

module.exports = {
    callback6
};
