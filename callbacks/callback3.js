/* 
	Problem 3: Write a function that will return all cards that 
    belong to a particular list based on the listID that is passed 
    to it from the given data in cards.json.
    Then pass control back to the code that called it by using a 
    callback function.
*/

// cards.json is an object
// using for in loop
// Note: Same function as callback2

function callback3(object, idData, cb) {
    setTimeout(() => {
        let data;
        for (let key in object) {
            if (key == idData) {
                data = object[key];
            }
        }

        if (data) {
            cb(null, data)
        } else {
            cb("No data received", null)
        }
    }, 2 * 1000);
}

module.exports = {
    callback3
};