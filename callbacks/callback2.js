/* 
	Problem 2: Write a function that will return all lists that belong to a board 
    based on the boardID that is passed to it from the given data in lists.json. 
    Then pass control back to the code that called it by using a callback function.
*/

// lists.json is an object
// using for in loop

function callback2(object, idData, cb) {
    setTimeout(() => {
        let data;
        for (let key in object) {
            if (key == idData) {
                data = object[key];
            }
        }

        if (data) {
            cb(null, data)
        } else {
            cb("No data received", null)
        }
    }, 2 * 1000);
}

module.exports = {
    callback2
};